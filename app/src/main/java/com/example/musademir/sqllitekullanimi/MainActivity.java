package com.example.musademir.sqllitekullanimi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    ListView lv;
    ArrayAdapter adapter;
    ArrayList<HashMap<String, String>> kullanici_liste;
    ArrayList<String> kullanici_adlari;
    ArrayList<Integer> kullanici_idler;
    Database db;
    EditText etemail;EditText etsifre;EditText etad;EditText etid;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //region Tanımlamalar
        Button btnEkle= (Button)findViewById(R.id.buttonEkle);
        Button btnSil= (Button)findViewById(R.id.buttonSil);
        Button btnGoruntule= (Button)findViewById(R.id.buttonGoruntule);
        Button btnDuzenle= (Button)findViewById(R.id.buttonDuzenle);
        etemail = (EditText)findViewById(R.id.editTextemail);
        etsifre  = (EditText)findViewById(R.id.editTextsifre);
        etad = (EditText)findViewById(R.id.editTextadsoyad);
        etid=(EditText)findViewById(R.id.editTextID);
        db=new Database(getApplicationContext());
        //endregion


        btnGoruntule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             tumunucek();//tüm verileri listele
            }
        });
        btnEkle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.kullaniciEkle(etemail.getText().toString(),etsifre.getText().toString(),etad.getText().toString());
                tumunucek();
            }
        });
        btnDuzenle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.kullaniciDuzenle(etemail.getText().toString()
                        ,etsifre.getText().toString(),etad.getText().toString(),Integer.parseInt(etid.getText().toString()));
                tumunucek();
            }
        });
        btnSil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                db.kullaniciSil(Integer.parseInt(etid.getText().toString())); tumunucek();
            }
        });
    }
    void tumunucek(){
        kullanici_liste=db.tumkullanicilar();//db.tumkullanicilar bize hashmap array döndürdü.
        if(kullanici_liste.size()==0){//kullanıcı listesi boşsa
            Toast.makeText(getApplicationContext(), "Henüz Kullanıcı Eklenmemiş.", Toast.LENGTH_LONG).show();
        }else {

            kullanici_adlari = new ArrayList<String>(); // kullanıcı adlarını tutucamız string arrayi olusturduk.
            kullanici_idler = new ArrayList<Integer>(); // kullanıcı id lerini tutucamız string arrayi olusturduk.
            for (int i = 0; i < kullanici_liste.size(); i++) {
                kullanici_adlari.add(kullanici_liste.get(i).get("ad"));
                kullanici_idler.add(Integer.parseInt(kullanici_liste.get(i).get("ID")));
              //arrayın i. elamanının ad ve ıdsini al
            }

            lv = (ListView) findViewById(R.id.listViewSonuc);

            adapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, android.R.id.text1, kullanici_adlari);
            lv.setAdapter(adapter);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                        long arg3) {

                    HashMap<String,String> map = db.kullaniciGetir((int) kullanici_idler.get(arg2));//kullanıcıid ve adları aynı sırayla çekildiği için aynı indekli ad ve idler aynı kullanıcıya ait o yüzden tıklanan indexi aldık(aslında listview sırasını aldık indexsimizle aynı) ve idlerde bu indexteki idyi getirdik daha sonra db den bu indexe ait verileri çektik gelen verileride edittextlere yazdırdık ki buradan seçim yapıp üzerinde değişiklik yapabilelim
                    etemail.setText(map.get("email"));
                    etad.setText(map.get("ad"));
                    etsifre.setText(map.get("sifre"));
                    etid.setText(map.get("ID"));//Id edittexti invisible bizim idmizi tutmamız için var görünürde lazım değil
                }
            });
        }
    }
}
