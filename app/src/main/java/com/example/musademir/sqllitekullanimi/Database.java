package com.example.musademir.sqllitekullanimi;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by MusaDemir on 8.9.2016.
 */
public class Database extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "sqllite_database";//database adı
    private static final String TABLE_NAME = "kullanici_listesi";//tablo adı
    private static String KULLANICI_ID = "ID";
    private static String KULLANICI_EMAIL = "email";
    private static String KULLANICI_SIFRE = "sifre";
    private static String KULLANICI_ADSOYAD = "ad";
    //bunları sürekli kullanacağımız için burada tanımladık ki kullanırken el ile yazmak yerine bu değişkenleri yerin koyup hata payını yokedelim



    public  Database(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {//Bu method veritabanımızı çağırdığımızda otamatik olarak çağırılacak
        String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + "("
                    + KULLANICI_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + KULLANICI_EMAIL + " TEXT,"
                + KULLANICI_SIFRE + " TEXT,"
                + KULLANICI_ADSOYAD + " TEXT" + ")";
        //String CREATE_TABLE="CREATE TABLE kullanici_listesi (
        // ID INTEGER PRIMARY KEY AUTOINCREMENT,
        // email TEXT,
        // sifre TEXT,
        // ad TEXT)";
        //iki stringde birbirine eş aslında kafa karışıklığını gidermek için oluşacak stringide paylaştım
        db.execSQL(CREATE_TABLE);// execSQL methodu ile bu stringi sql komutu olarak çalıştırdık

    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void kullaniciEkle(String email,String sifre,String adsoyad){
        SQLiteDatabase db= this.getWritableDatabase();// İlk önce ekleme yapacağımız için writable yani yazılabilir veritabanını çektik
        ContentValues values  = new ContentValues();// ContentValues SQLite ın olmazsa olmazıdır veri ve düzenlemede önce veriyi ContentValues olarak
        values.put(KULLANICI_EMAIL,email);// ekler daha sonrasında veritabanına ContentValues i kaydederiz
        values.put(KULLANICI_SIFRE,sifre);
        values.put(KULLANICI_ADSOYAD,adsoyad);
        db.insert(TABLE_NAME,null,values);
        db.close();
    }
    public void kullaniciDuzenle(String email, String sifre,String adsoyad,int id){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put(KULLANICI_EMAIL,email);
        cv.put(KULLANICI_SIFRE,sifre);
        cv.put(KULLANICI_ADSOYAD,adsoyad);
        db.update(TABLE_NAME,cv,KULLANICI_ID+" = ?",new String[] { String.valueOf(id) });
        //update methodu birinci parametre olarak düzenleme yapılacak tablo adını alır
        // ikinci parametre olarak duzenlenecek verileri alır biz burada ContentValues i verdik
        // üçüncü parametresi SQL deki WHERE komutudur ve bu where içindeki parametreler ? ile ifade edilir
        // örneğin buradaki WHERE ID=? dir ? nin ne olduğunu ise 4. parametrede belirtiriz
        // 4. Parametre WHERE ARgs yani parametreleri giriğimiz kısım , parametreleri birden fazla belirleyebileceğimiz için bir dizi oluşturup o diziyi paramtre olarak gönderiyoruz
        // Eğer birden fazla parametre kullanıcaksak normal sql sorgusu gibi and ile ayırırız Örneğin;
        //db.update(TABLE_NAME,cv,"ID = ? AND ad = ?",new String[] { String.valueOf(id), ad });
        db.close();
    }
    public void kullaniciSil(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, KULLANICI_ID + "= ?",new String[] { String.valueOf(id) } );// updatedeki where Clause buradada geçerli
        db.close();
    }
    public HashMap<String,String> kullaniciGetir(int id){// bir önceki yazıda gördüğümüz map sınıfı okumadıysanız öncelikle yazının alt kısmıdaki gereksinimler alanından o yazıyı okumanızı tavsiye ederim

        HashMap<String,String> kullanici = new HashMap<String,String>();

        String sqlsorgusu = "SELECT "+KULLANICI_ID+","+
                KULLANICI_EMAIL+","+
                KULLANICI_SIFRE+","+
                KULLANICI_ADSOYAD+
                " FROM "+TABLE_NAME+" Where "+KULLANICI_ID+"="+id;//SELECT ID,email,sifre,ad FROM kullanici_listesi Where ID = id

        SQLiteDatabase db= this.getReadableDatabase();// okunabilir database i al
        Cursor cursor = db.rawQuery(sqlsorgusu,null);

        cursor.moveToFirst();
        if(cursor.getCount()>0){
            kullanici.put(KULLANICI_ID,cursor.getString(0));// çekeceği 0. eleman yani ID
            kullanici.put(KULLANICI_EMAIL,cursor.getString(1));// 1 indisli eleman yani email
            kullanici.put(KULLANICI_SIFRE,cursor.getString(2));//hepsini hashmapa ekle
            kullanici.put(KULLANICI_ADSOYAD,cursor.getString(3));
        }
        cursor.close();
        db.close();

        return kullanici;// ve Hashmapi geri dönder

    }
    public ArrayList<HashMap<String,String>> tumkullanicilar(){// bu sefer id ye göre değil tüm kullanıcları seçicez
        // kullanıcılar için Hashmap kullanmıştık bu sefer birden çok kullanıcı olacak o yüzden bir Hashmap arraylist kullandık
        ArrayList<HashMap<String, String>> tumkullanicilarlist = new ArrayList<HashMap<String, String>>();

        SQLiteDatabase db = this.getReadableDatabase();
        String sql_sorgu="SELECT * FROM "+TABLE_NAME;
        Cursor cursor =  db.rawQuery(sql_sorgu,null);

        if(cursor.moveToFirst()){
            do {
                HashMap<String,String> gelenkullanici = new HashMap<String,String>();
                for (int i=0;i<cursor.getColumnCount();i++){//sutun sayısı kadar dönecek
                    gelenkullanici.put(cursor.getColumnName(i),cursor.getString(i));//i. sutunun adını anahtar olarak değerini value olarak ekliycek
                    //örneğin i =0 için  gelenkullanici.put("ID","gelendeğer"); i =1 için  gelenkullanici.put("email","gelendeğer"); gibi
                }

                tumkullanicilarlist.add(gelenkullanici);//hepsini arraya ekle

            }while(cursor.moveToNext());//ilerlemeye devam ettiği sürece döndür
            cursor.close();
            db.close();

        }
        return tumkullanicilarlist;// ve en son bu arraylisti geridöndür
    }
    public void resetTables(){
       //Tüm verileri siler. tabloyu resetler.
        SQLiteDatabase db = this.getWritableDatabase();

        db.delete(TABLE_NAME, null, null);
        db.close();
    }

}
